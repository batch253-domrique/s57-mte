let collection = [];

// Write the queue functions below.
//#1
function print() {
    return collection;
}
//#2
function enqueue(element) {
    collection.push(element);
    return collection;
}

//#4
function dequeue(element) {
    collection.shift(element);
    return collection;
}
//#7
function front() {
    return collection[0];
}
//#8
function size() {
    return collection.length;
}
//#9
function isEmpty() {
    if (collection.length === 0) {
        return true;
    } else {
        return false;
    }
}

module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty,
};
